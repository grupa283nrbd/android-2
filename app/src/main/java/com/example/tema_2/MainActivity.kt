package com.example.tema_2

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val users: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter = UsersAdapter(users)
    }

    fun addUser(view : View) {
            users.add(text_LastName.text.toString() + " " + text_FirstName.text.toString())
        recyclerView.adapter = UsersAdapter(users)
        UserRepository(this)
    }


}
