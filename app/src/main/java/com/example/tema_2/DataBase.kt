package com.example.tema_2

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class], version = 1)
abstract class Database : RoomDatabase() {
    public abstract fun userDao(): UserDao?

    abstract fun getDataBaseDao() : UserDao
}
