package com.example.tema_2

import android.content.Context
import android.os.AsyncTask


interface OnUserRepositoryActionListener {
    fun actionSuccess()
    fun actionFailed()
}

class UserRepository(context: Context?) {
    val appDatabase: Database = ApplicationController.getAppDatabase()

    fun insertUser(user: User?,
                   listener: OnUserRepositoryActionListener) {
        InsertTask(listener).execute(user)
    }

    fun getUserByName(firstName: String?, lastName: String?): User? {
        return appDatabase.userDao()?.findByName(firstName,lastName)
    }

    inner class InsertTask(val listener: OnUserRepositoryActionListener) : AsyncTask<User, Void, Void>(){
        override fun doInBackground(vararg users: User?): Void?{
            appDatabase.userDao()?.insertAll(users[0])
            return null
        }

        override fun onPostExecute(result: Void) {
            super.onPostExecute(result)
            listener.actionSuccess()
        }
    }
}




