package com.example.tema_2;

import android.app.Application;
import android.provider.SyncStateContract;

import androidx.room.Room;

public class ApplicationController extends Application {

    private static ApplicationController mInstance;

    private static Database mAppDatabase;

    public static ApplicationController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

        // Get a database instance to work with
        mAppDatabase = Room.databaseBuilder(getApplicationContext(),
                Database.class, "DataBase").build();
    }

    public static Database getAppDatabase(){
        return mAppDatabase;
    }
}
